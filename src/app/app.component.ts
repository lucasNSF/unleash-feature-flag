import { NgClass } from '@angular/common';
import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { UnleashService } from '@karelics/angular-unleash-proxy-client';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NgClass],
  template: `
    <h1>Welcome to {{title}}!</h1>
    <div class="square" [ngClass]="{ orange: coloredSquareFeature }"></div>
    @if (circleFeature) {
      <div class="circle"></div>
    }
    <router-outlet />
  `,
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'unleash_feature_flag';
  coloredSquareFeature: boolean | undefined;
  circleFeature: boolean | undefined;
  private unleashService = inject(UnleashService);
  private subscriptions: Subscription[] = [];

  ngOnInit(): void {
    this.unleashService.unleash.setContextField('userId', 'user-1');

    // this.coloredSquareFeature = this.unleashService.isEnabled('coloured-square');
    // this.circleFeature = this.unleashService.isEnabled('circle-element'); 

    const s1 = this.unleashService.isEnabled$('coloured-square').subscribe(value => {
      this.coloredSquareFeature = value;
    });
    const s2 = this.unleashService.isEnabled$('circle-element').subscribe(value => {
      this.circleFeature = value;
    });

    this.subscriptions.push(s1, s2);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
