import { ApplicationConfig, provideZoneChangeDetection } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideUnleashProxy } from '@karelics/angular-unleash-proxy-client';
import { routes } from './app.routes';

export const appConfig: ApplicationConfig = {
  providers: [
    provideZoneChangeDetection({ eventCoalescing: true }), 
    provideRouter(routes),
    provideUnleashProxy({
      url: 'http://localhost:3000/proxy',
      appName: 'development',
      clientKey: 'some-secret'
    }),
  ]
};
